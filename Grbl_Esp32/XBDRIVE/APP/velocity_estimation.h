#pragma once

#include <XBDRIVE/global.h>

void velocity_estimation_calculate();
int32_t get_vel_M();
int32_t get_vel_T();
int32_t get_vel();
int32_t get_vel_flt();
