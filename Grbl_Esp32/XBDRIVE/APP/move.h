#ifndef __MOVE_H_
#define __MOVE_H_

#include <XBDRIVE/global.h>

void Output(int32_t theta, int32_t effort);
void OneStep(void);

#endif